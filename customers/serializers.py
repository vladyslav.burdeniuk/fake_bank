from django.contrib.auth import get_user_model
from rest_framework import serializers


class CustomerSerializer(serializers.ModelSerializer):
    """Customer Serializer"""

    class Meta:
        model = get_user_model()
        fields = (
            "url",
            "username",
            "name",
        )
