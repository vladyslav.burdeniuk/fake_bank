import rest_framework_filters
from rest_framework import permissions

from core.atomic_viewsets import AtomicModelViewSet
from transactions.models import Transaction

from .serializers import TransactionSerializer


class TransactionViewSet(AtomicModelViewSet):  # pylint: disable=too-many-ancestors
    """Transaction ViewSet"""

    serializer_class = TransactionSerializer
    queryset = Transaction.objects.all()
    permission_classes = (permissions.IsAdminUser,)
    filter_backends = (rest_framework_filters.backends.ComplexFilterBackend,)
    filterset_fields = ("sender", "receiver")  # usage: transactions/?filters=(sender=1)%20|%20(receiver=1)
