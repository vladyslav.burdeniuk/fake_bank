from django.db.models.signals import post_delete, post_save, pre_save
from django.dispatch import receiver

from accounts.logic import balance_recount
from transactions.logic import check_enough_money_on_balance
from transactions.models import Transaction


@receiver(pre_save, sender=Transaction)
def transaction_pre_save(sender, instance, **kwargs):
    """Pre-save menthod"""
    check_enough_money_on_balance(instance)


@receiver(post_save, sender=Transaction)
def transaction_post_save(sender, instance, **kwargs):
    """Post-update method, uses for recount accounts balances"""
    balance_recount(instance.sender)
    balance_recount(instance.receiver)


@receiver(post_delete, sender=Transaction)
def transaction_post_delete(sender, instance, **kwargs):
    """Post-delete method, uses for recount accounts balances"""
    balance_recount(instance.sender)
    balance_recount(instance.receiver)
