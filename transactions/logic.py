from decimal import Decimal


def check_enough_money_on_balance(transaction):
    """Validate if account have enough money"""

    if transaction.sender_id and Decimal(transaction.sender.balance) < Decimal(transaction.amount):
        print(transaction.__dict__)
        raise ValueError(
            f"Not enough money on sender's balance: "
            f"sender.balance is {transaction.sender.balance}, you try to send {transaction.amount}"
        )
