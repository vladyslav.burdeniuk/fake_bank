from rest_framework import serializers

from transactions.models import Transaction


class TransactionSerializer(serializers.ModelSerializer):
    """Transaction Serializer"""

    class Meta:
        model = Transaction
        fields = (
            "id",
            "uid",
            "created",
            "amount",
            "comment",
            "sender",
            "receiver",
        )
