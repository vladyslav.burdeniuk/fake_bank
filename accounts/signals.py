from django.db.models.signals import post_save
from django.dispatch import receiver

from .logic import balance_recount, create_initial_transaction
from .models import Account


@receiver(post_save, sender=Account)
def account_post_save(sender, instance, created, **kwargs):
    if created:
        create_initial_transaction(instance)
    else:
        balance_recount(instance)
