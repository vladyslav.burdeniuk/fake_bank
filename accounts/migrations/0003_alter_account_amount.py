# Generated by Django 3.2.4 on 2021-06-09 08:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("accounts", "0002_account_customer"),
    ]

    operations = [
        migrations.AlterField(
            model_name="account",
            name="amount",
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=100, null=True),
        ),
    ]
