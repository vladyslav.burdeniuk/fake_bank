from decimal import Decimal

from django.db import models

from accounts.models import Account
from transactions.models import Transaction


def create_initial_transaction(account):
    """Create initial transaction with balance when account creates"""
    Transaction.objects.create(
        amount=account.balance,
        receiver=account,
        comment="Initial balance",
    )


def balance_recount(account):
    """Count balance from transactions"""
    if not account:
        return

    income = Transaction.objects.filter(receiver=account).aggregate(models.Sum("amount"))["amount__sum"] or Decimal(0.00)
    outcome = Transaction.objects.filter(sender=account).aggregate(models.Sum("amount"))["amount__sum"] or Decimal(0.00)
    # account.balance = income - outcome
    # account.save()

    # mass action to prevent post_update infinity cycle
    balance = income - outcome
    Account.objects.filter(id=account.id).update(balance=balance)
