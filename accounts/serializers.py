from rest_framework import serializers

from accounts.models import Account


class AccountSerializer(serializers.ModelSerializer):
    """Account Serializer"""

    class Meta:
        model = Account
        fields = (
            "id",
            "name",
            "balance",
            "created",
            "customer",
        )
