from rest_framework import permissions

from core.atomic_viewsets import AtomicModelViewSet

from .models import Account
from .serializers import AccountSerializer


class AccountViewSet(AtomicModelViewSet):  # pylint: disable=too-many-ancestors
    """Account ViewSet"""

    serializer_class = AccountSerializer
    queryset = Account.objects.all()
    permission_classes = (permissions.IsAdminUser,)
    filterset_fields = ("customer",)
