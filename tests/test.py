import os
import sys

import django

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "fake_bank.settings")
django.setup()

from django.contrib.auth import get_user_model  # noqa
from django.test import TestCase  # noqa

from accounts.models import Account  # noqa
from transactions.models import Transaction  # noqa


class ModelTest(TestCase):
    def setUp(self):
        self.user = get_user_model().objects.create_superuser(username="superuser", email="superuser@email.com", password="pass")
        self.user.is_superuser = True
        self.user.save(update_fields=["is_superuser"])

        self.testuser = get_user_model().objects.create(
            username="ArishaBarron",
            name="Arisha Barron",
        )

        self.testuser1 = get_user_model().objects.create(
            username="BrandenGibson",
            name="Branden Gibson",
        )
        self.account1 = Account.objects.create(name="TestAccount1", balance="1.0", customer=self.testuser)
        self.account2 = Account.objects.create(name="TestAccount2", balance="0", customer=self.testuser1)

    def testSuperuserCreate(self):
        self.assertEqual(get_user_model().objects.get(pk=1).username, "superuser")

    def testCustomerCreate(self):
        user = get_user_model().objects.filter(username="ArishaBarron")
        self.assertIsNotNone(user)

    def testAccountCreate(self):
        account1 = Account.objects.create(name="TestAccount", balance="1.0", customer=self.testuser)
        self.assertIsNotNone(account1)

    def testTransactionCreate(self):
        print(Account.objects.all())
        print(Transaction.objects.all())
        Transaction.objects.create(
            amount=1,
            sender=self.account1,
            receiver=self.account2,
        )
        print(Account.objects.all())
        print(Transaction.objects.all())
        self.assertEqual(self.account1.balance, 0)
        self.assertEqual(self.account2.balance, 1)

    def testTransactionNoEnoughMoneyCreate(self):
        no_enough_money = False
        try:
            Transaction.objects.create(
                amount=1,
                sender=self.account1,
                receiver=self.account2,
            )
        except ValueError:
            no_enough_money = True

        self.assertTrue(no_enough_money)
